from db import db
class Estudiantes(db.Model):
    __tablename__='estudiantes'
    id=db.column(db.integer , primary_key = True)
    nombre = db.column(db.string(50))
    email=db.column(db.string(70))
    codigo =db.column(db.string(15))

    def __init__(self,nombre,email,codigo):
        self.nombre=nombre
        self.email=email
        self.codigo=codigo
